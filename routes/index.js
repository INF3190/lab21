var express = require('express');
var router = express.Router();
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
	res.redirect('/lab21.html');
	
});

router.get('/univ', function(req, res, next) {
	var fhead=__dirname+"/../views/head.html";
	var head=fs.readFileSync(fhead, 'utf8');
	 /*fs.readFile(fhead, 'utf8', function (err,data) {
		  if (err) {
			    return console.log(err);
			  }
		     head=data;
		     //console.log(head);
			  return(data);
			});*/
	 console.log("dehors: "+head);
	var ftail=__dirname+"/../views/tail.html";
	var tail=fs.readFileSync(ftail, 'utf8');
	var content="";
	res.render('index', function (err, html) {
		if(err) throw err;
		//lecture fichier json
		var funiv=__dirname+"/../data/universites.json";
		var univdata=fs.readFileSync(funiv, 'utf8');
		    univjson=JSON.parse(univdata);
		    
		//création titre de niveau 1    
		 var h1="<h1 class='text-primary'>Universités</h1>";
		 content=content+h1;
		 //création table
		 var table="<table class='table table-striped'>";
		  table = table +"<thead><tr><th>Sigle</th><th>Nom</th></tr></thead>";
		  //insertion des données json dans la table
		  for (uni of univjson.liste){
			  //une ligne par université
			  var tr="<tr><td>"+uni.sigle+"</td><td>"+uni.nom+"</td></tr>";
			  table=table+tr;
			  
		  }
		 table = table + "</table>";//fermeture table
		 content = content + table;
		 console.log("toto "+head);
		  //content="<h1 class='text-primary'>Bonjour</h1>";
		  res.send(head+content+tail);
		  //res.send(html);
		})
});

//utilisation d'engin pug

router.get('/univpug', function(req, res, next) {
	var funiv=__dirname+"/../data/universites.json";
	var univdata=fs.readFileSync(funiv, 'utf8');
	    univjson=JSON.parse(univdata);
	var pagecontent={};
	 pagecontent.liste=univjson.liste;
	 pagecontent.pagetitle = 'Liste des universités';
	 pagecontent.contenttitle='Universités';
	 pagecontent.tabclass='table table-condensed table-hover';
	 console.log(pagecontent);
	res.render('universites.pug',pagecontent);
});
module.exports = router;
