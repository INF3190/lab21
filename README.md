# Laboratoire 21 - expressjs et template engine pug

- https://expressjs.com/en/guide/using-template-engines.html
- Affichage avec html simple
- Affichage avec template engine pug [https://pugjs.org](https://pugjs.org) (voir aussi 
http://jade-lang.com/) 
- https://github.com/tj/consolidate.js
- Manipulation des données Json (voir repertoire data)
- Utilisation bootstrap et css personnel
- Affichage des images avec bootstrap.
- Lecture des fichiers avec fs

## Installation

- Clonner l'application
- Installer les dépendances:
>~~~csh
> cd lab21
> npm install
>~~~

- Démarrer l'application
>~~~csh
>DEBUG=lab21:* npm start
>~~~
- Visiter les urls suivants:
> - http://localhost:3000/
> - http://localhost:3000/univ
> - http://localhost:3000/univpug      